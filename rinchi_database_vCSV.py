#!/usr/bin/env python

# NEW PYTHON SCRIPTS
# TESTING PHASE

# BENJAMIN HAMMOND 2014


import re, sys, sqlite3, argparse, csv, os
from collections import Counter
from ast import literal_eval

from rinchi_tools import conversion
import rinchi_rings

#######################################
# GENERATION AND CONVERSION OF DATABASES
#######################################

def convert_rdf_to_dict(rdf, header):
	""" Helper function accepts an RDFile and a list of parameters and returns a dict containing
	converted rinchis, keys, and reaction information
	"""
	with open(rdf) as data:
		input_data = data.read()
		
	# Set optional conversion parameters
	start_index = 0
	stop = 0
	
	# Set which columns to include
	return_rauxinfo = "RAuxInfo" in header
	return_longkey = "LongKey" in header
	return_shortkey = "ShortKey" in header
	return_rxninfo = "RXNInfo" in header

	
	# Run RInChI conversion functions.
	rinchidata = conversion.rdf_2_rinchis(input_data, start_index, stop, return_rauxinfo, return_longkey,
                                          return_shortkey, return_rxninfo)
	
	# Transpose nested list into a list of data entries
	data_transpose = map(list, zip(*rinchidata))
	
	# Force uniqueness	
	return {x[0]:x[1:] for x in data_transpose}

def rdf_to_csv(rdf, outfile=None, return_rauxinfo=False, return_longkey=False, return_shortkey=False, return_rxninfo=False):
	""" Takes an .rdf file as input and returns a .csv file containing RInChIs and other optional
		parameters
	"""

	#Check that input was supplied
	if not rdf:
		return None
		
	# Extract filename only
	input_name = rdf.split(".")[-2]
	input_name = input_name.split("/")[-1]
	
	header = ["RInChI"]
	if return_rauxinfo:
		header.append("RAuxInfo")
	if return_longkey:
		header.append("LongKey")
	if return_shortkey:
		header.append("ShortKey")
	if return_rxninfo:
		header.append("RXNInfo")	
	
	data_dict =  convert_rdf_to_dict(rdf, header)
	
	# Prevent overwriting, create output in an output folder in the current directory
	if not os.path.exists('output'):
		os.mkdir('output')
		
	# Set name of new file
	if outfile:
		new_name = os.path.join("output", outfile)
	else:
		new_name = os.path.join("output", input_name)
	
	# Add a number suffix if chosen filename already exists
	if os.path.exists('%s-rinchi.csv' %(new_name)):
		index = 1
		while os.path.exists('%s_%d-rinchi.csv' %(new_name, index)):
			index += 1
		output_name = '%s_%d-rinchi.csv' %(new_name, index)
	else:
		output_name = '%s-rinchi.csv' %(new_name)
	
	create_text_database
	# Write new database file as .csv
	with open(output_name, 'w') as f:
		writer = csv.writer(f,  delimiter='$')
		writer.writerow(header)
		writer.writerows([[i] + data_dict[i] for i in data_dict.keys()])
	return output_name

def rdf_to_csv_append(rdf, database):
	""" Takes an .rdf file and an existing .csv database and appends uniquely the reactions in the rdf file to the database
	"""
	
	# Initialise a list that will contain all the RInChIs currently in the database
	old_rinchis = []
	
	# Open the existing database and read the header defining which fields are present
	with open(database) as db:
		header = db.next().rstrip().split("$")
		reader = csv.reader(db, delimiter="$")
		# Add all rinchis in the existing database to a list
		for row in reader:
			old_rinchis.append(row[0])
	
	# Construct a dictionary of RInChIs and RInChI data from the supplied rd file
	new_data_dict = convert_rdf_to_dict(rdf, header)
	
	# Convert both lists of rinchis into sets - unique, does not preserve order
	old_rinchis = set(old_rinchis)
	new_rinchis = set(new_data_dict.keys())
	
	# The rinchis that need to be added to the database are the complement of the new rinchis in the old
	rinchis_to_add = list(new_rinchis - old_rinchis)
	
	# Add all new, unique rinchis to the database
	with open(database, "a") as db:
		writer = csv.writer(db,  delimiter='$')
		writer.writerows([[i] + new_data_dict[i] for i in rinchis_to_add])

def create_csv_from_directory(root_dir, outname, return_rauxinfo=False, return_longkey=False, return_shortkey=False, return_rxninfo=False):
	""" Given a root directory, an output filename, and a list of parameters, iterates recursively over
	all rdf files in the given folder and combines them into a single .csv database.
	"""
	
	# Flag for whether the database should be created or appended
	database_has_started = False
	
	# Iterate over all files in the roo directory
	for root, folders, files in os.walk(root_dir):
		for file in files:
			try:
				# Only try to process files with an .rdf extension
				if file.split(".")[-1] == "rdf":
					filename = os.path.join(root, file)
					if database_has_started:
						rdf_to_csv_append(filename, db_name)
					else:
						db_name = rdf_to_csv(filename, outname, return_rauxinfo, return_longkey, return_shortkey, return_rxninfo)
						database_has_started = True
			except IndexError:
				# Send the names of any files that failed to be recognised to STDOUT
				print filename

def rdf_to_sql(rdfile, db_filename, args=None):
	""" Creates or adds to an SQLite database the contents of a given RDFile. 
	"""
	db = sqlite3.connect(db_filename)
	cursor = db.cursor()
	
	cursor.execute('''
    CREATE TABLE IF NOT EXISTS 
    rinchis(
		rinchi TEXT, 
		longkey TEXT UNIQUE ON CONFLICT REPLACE, 
		rxninfo TEXT)
    ''')
	
	cursor.execute(''' PRAGMA main.page_size = 4096 ''')
	cursor.execute(''' PRAGMA main.cache_size=10000''')
	cursor.execute(''' PRAGMA main.locking_mode=EXCLUSIVE''')
	cursor.execute(''' PRAGMA main.synchronous=NORMAL''')
	cursor.execute(''' PRAGMA main.cache_size=5000''')
	
	# Open the rdfile and convert its contents to a dict of rinchis and rinchi data
	rdf_data = convert_rdf_to_dict(rdfile, ["RInChI", "LongKey", "RXNInfo"])
	
	# Transform in place the dicts storing rxn info to their string representations
	for i in rdf_data.keys():
		rdf_data[i][1] = repr(rdf_data[i][1])
	
	rdf_data_tuple = [tuple([i] + rdf_data[i]) for i in rdf_data.keys()]
		
	# Add the rdf data to the dictionary
	cursor.executemany( ''' INSERT INTO rinchis(rinchi, longkey, rxninfo) VALUES(?,?,?) '''
	, rdf_data_tuple)
	
	db.commit()
	
def sql_to_csv(db_filename, csv_name):
	db = sqlite3.connect(db_filename)
	cursor = db.cursor()
	
	with open(csv_name, 'rb') as csvfile:
		pass

def csv_to_sql(csv_name, db_filename):
	""" Opens a given dabase, or creates one if none exists, and appends the contents of a .csv file to the end, 
	as generated above.
	""" 
	db = sqlite3.connect(db_filename)
	cursor = db.cursor()
	
	cursor.execute('''
    CREATE TABLE IF NOT EXISTS 
    rinchis(
		rinchi TEXT, 
		longkey TEXT UNIQUE ON CONFLICT REPLACE, 
		rxninfo TEXT)
    ''')
	
	
	### May break on windows machines 
	cursor.execute(''' PRAGMA main.page_size = 4096 ''')
	cursor.execute(''' PRAGMA main.cache_size=10000''')
	cursor.execute(''' PRAGMA main.locking_mode=EXCLUSIVE''')
	cursor.execute(''' PRAGMA main.synchronous=NORMAL''')
	cursor.execute(''' PRAGMA main.journal_mode=WAL''')
	cursor.execute(''' PRAGMA main.cache_size=5000''')
	###
	
	with open(csv_name, 'rb') as csvfile:
		reader = csv.reader(csvfile, delimiter=",")
		reader.next()
		for row in reader:
			cursor.execute(''' INSERT INTO rinchis(rinchi, rxninfo, longkey, shortkey) VALUES(?,?,?,?) ''', row)
	db.commit()

#####################################
# SEARCHING OF DATABASES
#####################################

def sql_key_to_rxninfo(longkey, db_filename):
	""" Returns the $DATA/$DATUM field for the entry with the given long rinchi key
	"""
	db = sqlite3.connect(db_filename)
	cursor = db.cursor()
	
	cursor.execute('''SELECT rxninfo FROM rinchis WHERE longkey=?''', (longkey,))

	try:
		entry = cursor.fetchone()
		return literal_eval(entry[0])
	except KeyError, TypeError:
		print "Data not found"
		return None

def sql_key_to_rinchi(longkey, db_filename):
	""" Returns the RInChI matching the given Long RInChI key for a given database
	"""
	db = sqlite3.connect(db_filename)
	cursor = db.cursor()
	
	cursor.execute('''SELECT rinchi FROM rinchis WHERE longkey=?''', (longkey,))
	return cursor.fetchone()[0]

def search_for_inchi(inchi, db_filename):
	""" Searches for an inchi within a rinchi database. 
	Approx. 20x faster than the version in rinchi_tools.analyse
	"""
	
	db = sqlite3.connect(db_filename)
	cursor = db.cursor()
	
	query = "%" + "/".join(inchi.split("/")[1:]) + "%"
	
	cursor.execute('''SELECT rinchi FROM rinchis WHERE rinchi LIKE ?''', (query,))
	
	return [i[0] for i in cursor.fetchall()]
	
def ring_bitstring():
	pass
	
def advanced_search(db_filename, inchis=None, hyb={}, val={}, rings={}, formula={}):
	rinchis = search_for_inchi(inchis, db_filename)
	print len(rinchis), "inchi matches found"

	counter = 0
	for rin in rinchis:
		r = rinchi_rings.Reaction(rin)
		if r.detect_reaction(hyb_i=hyb, val_i=val, rings_i=rings, formula_i=formula):
			counter += 1
			print r.rinchi
	
	print counter, "exact matches found"

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="A collection of RInChI Tools - Benjamin Hammond 2014")
	parser.add_argument("input", help="Input - the RDFile or directory to be processed, or the search parameter for a search")
	parser.add_argument("database", nargs="?", help="The existing database to be modified or searched, or the name of new database to be created")
	parser.add_argument("arg3", nargs="?", help="optional arg 3")

	
	action = parser.add_mutually_exclusive_group(required=True)
	action.add_argument('--rdf2csv', action='store_true', help='Create a new .csv from an rdfile')
	action.add_argument('--rdfappend', action='store_true', help='Append the contents of an rdfile to an existing .csv file')
	action.add_argument('--dir2csv', action='store_true', help='Convert a directory of rdfiles to a single csv file')
	action.add_argument('--rdf2sql', action='store_true', help='Convert and add an rdfile to an SQL database')
	action.add_argument('--csv2sql', action='store_true', help='Add the contents of a rinchi .csv file to an SQL database')

	action.add_argument('--lkey2rinchi', action='store_true', help='Returns the RInChI corresponding to a given Long Key')
	action.add_argument('--lkey2rxninfo', action='store_true', help='Returns the RXNInfo for a given Long Key')
	action.add_argument('--inchisearch', action='store_true', help='Returns all RInChIs containing the given InChI to STDOUT')
	action.add_argument('--TEST', action='store_true', help='Returns all RInChIs containing the given InChI to STDOUT')


	args = parser.parse_args()
	
	
	if args.rdf2csv:
		rdf_to_csv(args.input, return_longkey=True, return_rxninfo=True)
	if args.rdfappend:
		rdf_to_csv_append(args.input, args.database)
	if args.dir2csv:
		create_csv_from_directory(args.input, args.database, return_longkey=True, return_rxninfo=True)
	if args.rdf2sql:
		rdf_to_sql(args.input, args.database)
	if args.csv2sql:
		csv_to_sql(args.input, args.database)
		
	if args.TEST:
		advanced_search(args.database, args.input, hyb={"sp2":-4, "sp3":4}, rings={6:1})
		
		
	if args.lkey2rinchi:
		print sql_key_to_rinchi(args.input, args.database)
	if args.lkey2rxninfo:
		print sql_key_to_rxninfo(args.input, args.database)
	if args.inchisearch:
		for rin in search_for_inchi(args.input, args.database):
			print rin
		
