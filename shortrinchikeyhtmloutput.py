# -*- coding: utf-8 -*-
# RInChI Project
# James Apthorp
# Generates HTML pages containing 1000 short RInChI keys from the RInChI database

import sqlite3

conn = sqlite3.connect('rinchi_oct_2015.db')
c = conn.cursor()

for x in range(835):
    number = int(1000 * x)
    c.execute("SELECT shortkey FROM rinchiwithkeys LIMIT 1000 OFFSET {}".format(str(number)))
    page = c.fetchall()
    page_base = 'page{}.html'

    with open(page_base.format(str(x)),'w+') as f:
        f.write("Page".format(str(x)))
        for key in page:
            print(key)
            f.write(key[0]+ ' <br>',)

for x in range(835):
    with open(page_base.format(str(x)),'r+') as f:
            f.write('''
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet"
href="http://www-jmg.ch.cam.ac.uk/global/style/layout.css" type="text/css"
media="all" />
<link rel="stylesheet"
href="http://www-jmg.ch.cam.ac.uk/global/style/print.css" type="text/css"
media="print" />
<link rel="Shortcut Icon"
href="http://www.cam.ac.uk/templates/favicon.ico" /><!--[if IE 6]><link
rel="stylesheet" href="http://www-jmg.ch.cam.ac.uk/global/style/ie6.css"
type="text/css" media="screen" /><script type="text/javascript"
src="http://www-jmg.ch.cam.ac.uk/global/js/minmax.js"></script><![endif]--><!--[if
IE 7]><link rel="stylesheet"
href="http://www-jmg.ch.cam.ac.uk/global/style/ie7.css" type="text/css"
media="screen" /><![endif]-->
<title>
Plaintext RInChI Database
</title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" type="text/css"
href="http://www-jmg.ch.cam.ac.uk/global/style/research.css" />
</head>
<body class="one-col">

<div id="dept-title">
<h1 align="center">
&nbsp;&mdash;&nbsp;
<a href="http://www-jmg.ch.cam.ac.uk/">Goodman Group</a>
&nbsp;&mdash;&nbsp;</h1>
<center>
<a href="http://www.ch.cam.ac.uk/">Department of Chemistry</a>
&nbsp;&mdash;&nbsp;
<a href="http://www.cam.ac.uk/">University of Cambridge</a>
<p />
</center>
</div>
<div id="container"> <a name="skip-content" id="skip-content"></a>

<h1 align="center">1000 Short RInChI Keys</h1>
<p>
''')

for x in range(835):
    with open(page_base.format(str(x)),'a+') as f:
            f.write('''
</p>
<ul id="site-info">
<li class="copy">&copy; 2008-2015 Jonathan Goodman, University of Cambridge, Department of Chemistry, Lensfield Road, Cambridge, CB2 1EW<br />
      Information provided by <a
href="mailto:jmg11@cam.ac.uk">jmg11@cam.ac.uk</a></li>
    <li class="link last"><a
href="http://www.ucs.cam.ac.uk/privacy">Privacy</a></li>
    <li class="link last"><a
href="http://www-rinchi.ch.cam.ac.uk/website2015/index.html">RInChI Project</a></li>
  </ul>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69131766-2', 'auto');
  ga('send', 'pageview');

</script>
</div>
</body></html>
''')

conn.close