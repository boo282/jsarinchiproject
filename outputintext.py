# -*- coding: utf-8 -*-
# RInChI Project
# James Apthorp
# Generates TXT file containing all RInChIs from the RInChI database

# Adapted from Python script written to output HTML files from the RInChI database, so some code is redundant.

import sqlite3

conn = sqlite3.connect('rinchi.db')
c = conn.cursor()

for x in range(1):
    number = int(1000000 * x)
    c.execute("SELECT rinchi FROM rinchis LIMIT 1000000 OFFSET {}".format(str(number)))
    page = c.fetchall()
    page_base = 'page{}.txt'

    with open(page_base.format(str(x)),'w+') as f:
        f.write("".format(str(x)))
        for key in page:
            print(key)
            f.write(key[0]+ '\n',)

conn.close
