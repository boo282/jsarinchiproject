<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<!-- remove next line if your web server adds charset-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" 
href="http://www-jmg.ch.cam.ac.uk/global/style/layout.css" type="text/css" 
media="all" />
<link rel="stylesheet" 
href="http://www-jmg.ch.cam.ac.uk/global/style/print.css" type="text/css" 
media="print" />
<link rel="Shortcut Icon" 
href="http://www.cam.ac.uk/templates/favicon.ico" /><!--[if IE 6]><link 
rel="stylesheet" href="http://www-jmg.ch.cam.ac.uk/global/style/ie6.css" 
type="text/css" media="screen" /><script type="text/javascript" 
src="http://www-jmg.ch.cam.ac.uk/global/js/minmax.js"></script><![endif]--><!--[if 
IE 7]><link rel="stylesheet" 
href="http://www-jmg.ch.cam.ac.uk/global/style/ie7.css" type="text/css" 
media="screen" /><![endif]-->
<title>
RInChI
</title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" type="text/css" 
href="http://www-jmg.ch.cam.ac.uk/global/style/research.css" />
</head>
<body class="one-col">
<div id="skip"> <a href="#skip-content" accesskey="2">Skip to 
content</a>&nbsp;|&nbsp;<a 
href="http://www.cam.ac.uk/site/accesskeys.html" 
accesskey="0">Access key help</a> </div><div id="header">
  <div id="branding"><a href="http://www.cam.ac.uk/" accesskey="1"><img 
src="http://www-jmg.ch.cam.ac.uk/global/images/identifier800.gif" 
alt="University of Cambridge" class="ucam" /></a>
</div>
<div id="branding">
<a href="http://www-jmg.ch.cam.ac.uk/"><img 
src="http://www-jmg.ch.cam.ac.uk/images/cuclii09a.jpg" alt="Chemistry 
Department Home" class="ucam"/></a>
  </div>

    <div id="site-search">
    
       <form 
action="http://web-search.cam.ac.uk/query.html?qp=site%3ach.cam.ac.uk" 
method="get">
      <fieldset>
      <label for="search-term">Search</label>
      <input name="qt" type="text" id="search-term" accesskey="4" 
value="" />
      <input type="hidden" name="qp" value="site:ch.cam.ac.uk" />
     <input id="search-button" 
src="http://www-jmg.ch.cam.ac.uk/global/images/button-search.gif" 
value="Search" alt="Search" title="Search" type="image" />
      </fieldset>
    </form>
    
    <ul><!-- this is marked up in reverse order as the list is floated 
right -->
    <li><a href="http://www-jmg.ch.cam.ac.uk/search.html">Advanced 
search</a></li>
    <li><a href="http://www-jmg.ch.cam.ac.uk/contact.html">Contact 
us</a></li>
  </ul>
  
  </div><!-- #site-search ends -->
</div>

<div id="dept-title">
<h1><a href="http://www-jmg.ch.cam.ac.uk/">Department of Chemistry</a></h1>
</div> 
<div id="navtabs">
<ul>
<li><a href="http://www-jmg.ch.cam.ac.uk/">Home</a></li>
<li><a href="http://www-jmg.ch.cam.ac.uk/staff/">Academic Staff</a></li>
<li><a href="http://www-jmg.ch.cam.ac.uk/events/">Colloquia</a></li>
<li><a href="http://www-jmg.ch.cam.ac.uk/research.html">Research</a></li>
<li><a href="http://www-jmg.ch.cam.ac.uk/resources/">Resources</a></li>
<li><a href="http://www-teach.ch.cam.ac.uk/">Teaching</a></li>
<li><a href="http://www-jmg.ch.cam.ac.uk/intranet.html">Intranet</a></li>
</ul>
</div>
<div id="container"> <a name="skip-content" id="skip-content"></a>

<ul id="nav-breadcrumb"> <li class="first"><a 
href="http://www.cam.ac.uk/">University of Cambridge</a></li> <li><a 
href="http://www.ch.cam.ac.uk/">Department of Chemistry</a></li><li><a 
href="http://www-rinchi.ch.cam.ac.uk">RInChIs</a></li>
</ul>
<div id="sub-brand"><p class="section">RInChI</p></div>
<div id="content">

<h1>Long Key to RInChI</h1>

<?php
$time = microtime(true);
$dot = array(".");
$time = str_replace($dot, "", $time);
$filename = "uploads/rinchi".$time;
$file = fopen($filename, "w+");

echo '<textarea rows="10" cols="80">';

if($_POST['lkey']) {
	
    $input = $_POST['lkey'];
    $linebreaks = array("\r\n", "\n\r", "\r", "\n");
    $input = escapeshellarg(str_replace($linebreaks, "\n", $input));
    fwrite($file, $input);

    fclose($file);

    exec("./rinchi_database.py --lkey2rinchi $input rinchi.db", $output);
	
	if (empty($output)) {
		echo 'Long RInChI Key not found!';
	}
	
    foreach ($output as $line) {
        echo $line;
        echo "\n";
        
        
    }
    
}

else {

echo "Long RInChI Key not found!";

}

echo "</textarea>";

exec("rm -f $filename")


?>


  </div>
  <ul id="site-info">
    <li class="copy">&copy; 2008-2011 University of Cambridge, 
Department of Chemistry, Lensfield Road, Cambridge, CB2 1EW<br />
      Information provided by <a 
href="mailto:jmg11@cam.ac.uk">jmg11@cam.ac.uk</a></li>
    <li class="link last"><a 
href="http://www.ch.cam.ac.uk/privacy.html">Privacy</a></li>
    <li class="link"><a 
href="http://www.cam.ac.uk/site/">Accessibility</a></li>
  </ul>
</div>
</body></html>











