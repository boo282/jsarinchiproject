
#!/usr/bin/env python

"""RInChI Conversion Script.

    Copyright 2012 C.H.G. Allen

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.


This script is able to convert between RXNfiles and RInChIs; it can also
convert reactions from RDfiles to RInChIs, while attempting to parse additional
data (currently limited to variations of catalysts and solvents) from the file.

Modifications by James Apthorp in March 2016 denoted by #JAMES APTHORP MODIFICATION MARCH 2016

RXNfile-to-RInChI conversion:

    Invoked by having -rxn2rinchi as the script's first agument.
    
    Sample use:
        ./rinchi_convert.py -rxn2rinchi /some/path/myrxnfile.rxn -options
        
    Options:
        -rauxinfo
            Generate and return RAuxInfo along with the RInChI.
        -longkey
            Generate and return the Long-RInChIKey along with the RInChI.
        -shortkey
            Generate and return the Short-RInChIKey along with the RInChI.
        -fout
            Save the output to disk, rather than printing to the terminal.


RInChI-to-RXNfile conversion:

    Invoked by having -rinchi2rxn as the script's first agument.

    Sample use:
        ./rinchi_convert.py -rinchi2rxn /some/path/myrinchi.rinchi -options
        
    Options:
        -stdout
            Print the RXNfile to the terminal, rather than saving to disk. 
        

RDfile-to-RInCHI conversion:

    Invoked by having -rdf2rinchi as the script's first agument.
    
    Due to RDfiles often containing a great many RXN entries, this function
    always saves the RInChIs (and other outputs) to disk in the output folder.

    Sample use:
        ./rinchi_convert.py -rdf2rinchi /some/path/myrdfile.rdf -options
    
    Options:
        -rauxinfo
            Generate and return RAuxInfos along with the RInChIs.  These
            RAuxInfos will be saved to a seperate .rinchi file.
        -longkey
            Generate and return the Long-RInChIKeys along with the RInChIs.
            These RAuxInfos will be saved to a seperate .rinchi file.
        -shortkey
            Generate and return the Short-RInChIKeys along with the RInChIs.
            These RAuxInfos will be saved to a seperate .rinchi file.
        -begin:n
            Begin conversion at the nth RXN record.
        -end:m
            End conversion at the mth RXN record.     
"""


import os
import sys

from rinchi_tools import conversion
from rinchi_tools import rinchikey


def __rxn2rinchi(args):
    """Called when -rxn2rinchi is given as the 1st argument of the script."""
    # Check that an RXNfile is given.
    if not args:
        print "Please specify an RXNfile for conversion."
        return
    # Parse RXNfile input.
    input_path = args[0]
    input_name = input_path.split('/')[-1].split('.')[0]
    try:
        input_rxn = open(input_path).read()
    except IOError:
        print 'Could not open file "%s".' %(input_path)
        return
    # Parse optional arguments
    return_rauxinfo = False
    return_longkey = False
    return_shortkey = False
    file_out = False
    for arg in args[1:]:
        if arg.startswith('-r'):
            return_rauxinfo = True
        if arg.startswith('-l'):
            return_longkey = True
        if arg.startswith('-s'):
            return_shortkey = True
        if arg.startswith('-f'):
            file_out = True
    # Generate the requested data.
    if return_rauxinfo:
        out_rinchi, rauxinfo = conversion.rxn_2_rinchi(input_rxn,
                                                       return_rauxinfo=True)
    else:
        out_rinchi = conversion.rxn_2_rinchi(input_rxn)
    if return_longkey:
        longkey = rinchikey.rinchi_2_longkey(out_rinchi)
    if return_shortkey:
        shortkey = rinchikey.rinchi_2_shortkey(out_rinchi)
    # If requested, save the output to a file.
    if file_out:
        # Ensure an output directory exists.
        if not os.path.exists('output'):
            os.mkdir('output')
        os.chdir('output')
        # Prevent overwriting.
        if os.path.exists('%s.rinchi' %(input_name)):
            index = 1
            while os.path.exists('%s_%d.rinchi' %(input_name, index)):
                index += 1
            output_name = '%s_%d.rinchi' %(input_name, index)
        else:
            output_name = '%s.rinchi' %(input_name)
        # Write to disk
        output_file = open(output_name, 'w')
        output_file.write(out_rinchi)
        if return_rauxinfo:
            output_file.write('\n' + rauxinfo)
        if return_longkey:
            output_file.write('\n' + longkey)
        if return_shortkey:
            output_file.write('\n' + shortkey)
        output_file.close()
    # Otherwise, print the output.
    else:
        print out_rinchi
        if return_rauxinfo:
            print rauxinfo
        if return_longkey:
            print longkey
        if return_shortkey:
            print shortkey
    return


def __rinchi2rxn(args):
    """Called when -rinchi2rxn is given as the 1st argument of the script."""
    # Check that a RInChI file is given.
    if not args:
        print 'Please specify a RInChI file for conversion.'
        return
    # Parse RInChI file input.
    input_path = args[0]
    input_name = input_path.split('/')[-1].split('.')[0]
    try:
        input_file = open(input_path)
    except IOError:
        print 'Could not open file "%s".' %(input_path)
        return
    input_rinchi = ''
    input_auxinfo = ''
    for line in input_file.readlines():
        if line.startswith('RInChI'):
            input_rinchi = line.strip()
        elif line.startswith('RAux'):
            input_auxinfo = line.strip()
    input_file.close()
    # Parse optional arguments.
    std_out = False
    for arg in args[1:]:
        if arg.startswith('-s'):
            std_out = True
    # Generate RXN file.
    rxn = conversion.rinchi_2_rxn(input_rinchi, input_auxinfo)
    rxn = '$RXN\n' + input_name + '\n' + rxn.split('\n', 2)[2]
    # If specified, print the output.
    if std_out:
        print rxn
    # Otherwise, save to a file.
    else:
        # Ensure an output directory exists.
        if not os.path.exists('output'):
            os.mkdir('output')
        os.chdir('output')
        # Prevent overwriting.
        if os.path.exists('%s.rxn' %(input_name)):
            index = 1
            while os.path.exists('%s_%d.rxn' %(input_name, index)):
                index += 1
            output_name = '%s_%d.rxn' %(input_name, index)
        else:
            output_name = '%s.rxn' %(input_name)
        # Write to disk
        output_file = open(output_name, 'w')
        output_file.write(rxn)
        output_file.close()
    return


def __rdf2rinchi(args):
    """Called when -rinchi2rxn is given as the 1st argument of the script."""
    # Check that an RDfile is specified.
    if not args:
        print "Please specify an RDfile for conversion."
        return
    # Parse RDfile input.
    input_path = args[0]
    input_name = input_path.split('/')[-1].split('.')[0]
    try:
        input_file = open('%s' %input_path).read()
    except IOError:
        print 'Could not open file "%s".' %(input_path)
        return
    # Parse optional arguments.
    return_rauxinfo = False
    return_longkey = False
    return_shortkey = False
    start = 0
    stop = 0
    for arg in args[1:]:
        if arg.startswith('-r'):
            return_rauxinfo = True
        if arg.startswith('-l'):
            return_longkey = True
        if arg.startswith('-s'):
            return_shortkey = True
        if arg.startswith('-begin:'):
            start = int(arg[7:])
        if arg.startswith('-end:'):
            stop = int(arg[5:])
    # Run RInChI conversion functions.
    if start:
        start_index = start - 1
    else:
        start_index = start
    rinchidata = conversion.rdf_2_rinchis(input_file, start_index, stop,
                                          return_rauxinfo, return_longkey,
                                          return_shortkey)
    # Ensure an output directory exists.
    if not os.path.exists('output'):
        os.mkdir('output')
    os.chdir('output')
    # Label output with start and stop values.
    output_name = input_name
    if start:
        if stop > start:
            output_name += '_%dto%d' %(start, stop)
        elif stop < start:
            output_name += '_from%d' %(start)
        elif start == stop:
            output_name += '_%donly' %(start)
    elif stop:
        output_name += '_upto%d' %(stop)
    # Prevent overwriting.
    if os.path.exists(output_name + '.rinchi'):
        index = 1
        while os.path.exists('%s_%d.rinchi' %(output_name, index)):
            index += 1
        output_name = '%s_%d' %(output_name, index)
    # Write to disk.

    # JAMES APTHORP MODIFICATION MARCH 2016: RD FILE TO CSV CONVERSION

    rinchis = rinchidata[0]
    rinchidata = rinchidata[1:]
    rinchi_file = open(output_name + '.csv', 'a')

    for n in xrange(0, len(rinchis)):
         
         LOG = 100
       
         if (n % LOG) == 0:
         	print n

         rinchi_file.write("\"" + rinchis[n] + "\",\"" + rinchidata[0][n] + "\",\"" + rinchidata[1][n] + "\",\"" + rinchidata[2][n] + "\"")
         rinchi_file.write('\n')
	 rinchi_file.close

    #for rinchi_entry in rinchis:
    #    rinchi_file.write("\"" + rinchi_entry + "\"",)
    #for longkey in longkeys:
    #    rinchi_file.write("\"longkey\"",)
    #for shortkey in shortkeys:
    #    rinchi_file.write("\"shortkey\"")
    #    rinchi_file.write('\n')
    rinchi_file.close()
    #if return_rauxinfo:
    #    rauxinfos = rinchidata[0]
    #    rinchidata = rinchidata[1:]
    #    rauxinfo_file = open(output_name + '_rauxinfo.rinchi', 'a')
    #    for rauxinfo in rauxinfos:
    #        rauxinfo_file.write(rauxinfo)
    #        rauxinfo_file.write('\n')
    #    rauxinfo_file.close()
    #if return_longkey:
    #    longkeys = rinchidata[0]
    #    rinchidata = rinchidata[1:]
    #    longkey_file = open(output_name + '_longkeys.rinchi', 'a')
    #    for longkey in longkeys:
    #        longkey_file.write(longkey)
    #        longkey_file.write('\n')
    #    longkey_file.close()
    #if return_shortkey:
    #    shortkeys = rinchidata[0]
    #    rinchidata = rinchidata[1:]
    #    shortkey_file = open(output_name + '_shortkeys.rinchi', 'a')
    #    for shortkey in shortkeys:
    #        shortkey_file.write(shortkey)
    #        shortkey_file.write('\n')
    #    shortkey_file.close()
    return


if __name__ == "__main__":
    if len(sys.argv) > 1:
        command_line_args = sys.argv[2:]
        if sys.argv[1] == '-rxn2rinchi':
            __rxn2rinchi(command_line_args)
        elif sys.argv[1] == '-rinchi2rxn':
            __rinchi2rxn(command_line_args)
        elif sys.argv[1] == '-rdf2rinchi':
            __rdf2rinchi(command_line_args)
        else:
            print 'First argument must be one of:'
            print '-rxn2rinchi: To convert an RXNfile to a RInChI.'
            print '-rinchi2rxn: To convert a RInChI to an RXNfile.'
            print '-rdf2rinchi: To convert an RDfile to RInChI(s).'
    else:
        print __doc__
